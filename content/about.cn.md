---
title: "主页"
date: 2018-10-21T11:12:08-04:00
draft: true
---

你好，我叫Nico Boekschoten！中国人可以叫我凯健。
我是美国人，可是我七年学习了中文。现在我太荒疏，所以要是我写错了，请告诉我。

现在我是计算机工程本科生在北卡罗莱纳州立大学，打算5月2019年毕业。
毕业以后，我要两年工作，以后回大学收到硕士学位。

我的技术兴趣包括：

* 嵌入式系统

* 数字工程

* 并行计算

* 机器学习

要是你想联络我，请发我电子邮件在：nico [at] nicohb [dot] com

谢谢！
