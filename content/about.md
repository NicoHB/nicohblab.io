---
title: "About"
date: 2019-09-29T11:12:08-04:00
draft: false
---

Hello, I am Nico Boekschoten, welcome to my website!

I am currently a Master's Computer Engineering student at North Carolina State University. I will have 1 class remaining for my degree after the spring 2020 semester, and am looking for either a full time position or internship for the summer/fall. 

My main area of interest is computer architecture and digital hardware design, I would love to develop processors, GPUs, or ASICs. I have a strong background in hardware architecuture, design and verification. 

If you would like to contact me, please send an email to: nico [at] nicohb [dot] com

Thanks!




