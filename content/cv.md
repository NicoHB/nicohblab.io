---
title: "Curriculum Vitae"
date: 2019-09-29T11:12:08-04:00
draft: false
---


Education:

North Carolina State University:  Masters of Computer Engineering, July/December 2020 Intended

Ongoing Courses:

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 748: ASIC Verification with Universal Verification Methodology (UVM)

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 792: Quantum Computing Architectures

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 592: Cryptographic Engineering and Hardware Security

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 542: Neural Networks

Completed Courses:

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 566: Compiler Optimization

&nbsp;&nbsp;&nbsp;&nbsp; - ECE 506: Architecture Of Parallel Computers

<br>

<br>

<br>

North Carolina State University:  Bachelors of Computer Engineering, Graduated May 2019

Courses:

&nbsp;&nbsp;&nbsp;&nbsp;Electrical and Computer Engieering:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 745: ASIC Verification

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 461: Embedded System Architectures and Optimization

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 485: Senior Design II

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 464: Digital ASIC Design

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 463: Microprocessor Architecture

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 460: Embedded System Design

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 484: Senior Design I

<br>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 301: Linear Systems

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 302: Microelectronics

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 303: Electromagnetic Fields

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 306: Introduction to Embedded Systems

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 309: Object-Oriented Programming (Java)

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ECE 310: Design of Complex Digital Systems

&nbsp;&nbsp;&nbsp;&nbsp;Other relevant:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Discrete Math

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Statistics

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Communication in Engineering and Technology

		